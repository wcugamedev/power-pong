TODO List
===

  - Make some power ups? LIST YOUR IDEAS:
    - ~~Shrinkage: paddle shrinks 25% in size~~
    - Growth: paddle grows 20% in size
    - ???
  - Make utility for scripted UI (in other words, UI should be written with
    XML or JSON and interpreted live)
  - I know this is tedious, but I forgot to do type checks everywhere...
  - ~~Better AI (use old pong as template)~~
  - ~~Make splash screen nicer~~ No splash screen
  - ~~Fix splash screen being boring~~ No splash screen
  - Documentate :(
