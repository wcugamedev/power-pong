"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/26/14
"""
import random
from pygame.rect import Rect
from powerups.PowerUp import Shrinkage


class PowerUpManager:
    def __init__(self, player_manager, ball, play_area):
        self.player_manager = player_manager
        self.play_area = play_area
        self.ball = ball
        self.power_ups = []
        self.power_up_timer = 0
        self.next_power_up = random.randint(100, 1000)

    def update(self):
        if self.power_up_timer < self.next_power_up:
            self.power_up_timer += 1
        else:
            self.power_up_timer = 0
            self.next_power_up = random.randint(100, 1000)
            self.add_random_power_up()
        for p in self.power_ups:
            p.update()
            if not p.active:
                r1 = Rect((p.x, p.y, p.width, p.height))
                r2 = Rect((self.ball.x, self.ball.y, self.ball.width,
                           self.ball.height))
                if r2.colliderect(r1):
                    # Ball intersects
                    if self.ball.vel_x < 0:
                        recipient = self.player_manager.player2
                    else:
                        recipient = self.player_manager.player1
                    p.activate(recipient)
            if p.expired:
                self.power_ups.remove(p)

    def add_random_power_up(self):
        power_ups = [Shrinkage()]
        choice = random.choice(power_ups)
        x = random.randint(self.play_area.x + choice.width,
                           self.play_area.x + self.play_area.width -
                           choice.width)
        y = random.randint(self.play_area.y + choice.height,
                           self.play_area.y + self.play_area.height -
                            choice.height)
        choice.x = x
        choice.y = y
        self.power_ups.append(choice)

    def render(self, display):
        for p in self.power_ups:
            p.render(display)