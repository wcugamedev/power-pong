"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/26/14
"""
import pygame

POWER_UP_IMAGES = {
    'SHRINKAGE': pygame.image.load('../res/expander.png')
}


class PowerUp:
    def __init__(self, **kwargs):
        x = kwargs['x'] if 'x' in kwargs else 0
        y = kwargs['y'] if 'y' in kwargs else 0
        width = kwargs['width'] if 'width' in kwargs else 30
        height = kwargs['height'] if 'height' in kwargs else 30
        image = kwargs['image'] if 'image' in kwargs else None
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.image = image
        self.duration = 500
        self.timer = 0
        self.player_affected = None
        self.expired = False
        self.active = False
        self.scaled_image = None
        self.non_active_duration = 500
        self.non_active_timer = 0

    def activate(self, player):
        self.player_affected = player
        self.active = True
        self.on_start(player)

    def render(self, display):
        if not self.active:
            if not self.scaled_image:
                self.scaled_image = pygame.transform.scale(self.image, (
                    self.width, self.height
                ))
            display.blit(self.scaled_image, (self.x, self.y))

    def update(self):
        if self.active:
            if self.timer < 500:
                self.timer += 1
            else:
                self.timer = 0
                self.expired = True
                self.on_end(self.player_affected)
        else:
            if self.non_active_timer < self.non_active_duration:
                self.non_active_timer += 1
            else:
                self.timer = 0
                self.expired = True

    def on_end(self, player):
        raise NotImplementedError()

    def on_start(self, player):
        raise NotImplementedError()


class Shrinkage(PowerUp):
    def __init__(self, **kwargs):
        PowerUp.__init__(self, image=POWER_UP_IMAGES['SHRINKAGE'],**kwargs)

    def on_start(self, player):
        player.height -= int(player.height * .25)

    def on_end(self, player):
        player.height += int(player.height * .25)