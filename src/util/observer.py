"""
This module contains interfaces for a generic subject-observer pattern.

:author: Anthony Benavente
:date: 12/23/14
"""


class Observer:
    """
    This class represents an Observer in the observer pattern. This only has
    one method that needs to be implemented by any implementing classes.
    """
    def notify(self, evt_args):
        """
        A method to be called when the observer has been notified.

        :param evt_args: Any object that contains necessary information for this
            observer
        :return: None
        """
        raise NotImplementedError()


class Subject:
    """
    This class represents a Subject in the observer pattern. It comes with a
    built-in constructor that must be called to initialize the observer list.
    It also provides utilities for adding, removing, and notifying observers.
    """
    def __init__(self):
        self.observers = []

    def add(self, o):
        """
        Adds an observer to the list of observers

        :param o: The observer to add (must be a class implementing the
            observer class)
        """
        self.observers.append(o)

    def remove(self, o):
        """
        Removes the passed in observer if it exists in the list, otherwise, it
        will print out a message.

        :param o: The observer to remove.
        """
        if o in self.observers:
            self.observers.remove(o)
        else:
            print("Observer not in list of observers")

    def notify_all(self):
        """
        A method that must be implemented that represents how all observers
        will be notified. This must be implemented because the event arguments
        will vary for different subjects/observers.
        """
        raise NotImplementedError()