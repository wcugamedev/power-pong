"""
This module contains a utility for converting a hex number to an (r, g, b)
tuple.

:author: Anthony Benavente
:date: 10/30/2014
"""

def hex_to_color(color):
    """
    Converts a hexadecimal number into a tuple containing (r, g, b) values
    where 0 <= r, g, b <= 255.

    :param color: The color as a hexadecimal number
        (i.e. 0x00ff00 == (0, 255, 0))
    :return: A tuple containing the values (r, g, b)
    """
    r = (color & 0xff0000) >> 16
    g = (color & 0x00ff00) >> 8
    b = (color & 0x0000ff)
    return r, g, b


# Below are some definitions for predefined colors
red = hex_to_color(0xff0000)
green = hex_to_color(0x00ff00)
blue = hex_to_color(0x0000ff)
alice_blue = hex_to_color(0xF0F8FF)
coral = hex_to_color(0xFF7F50)
cornflower_blue = hex_to_color(0x6495ED)
pink = hex_to_color(0xFFC0CB)
white = hex_to_color(0xffffff)
black = hex_to_color(0x000000)
yellow = hex_to_color(0xffff00)