"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/2/15
"""
from sys import stdin
import wx

from ui.ControlManager import ControlManager
from ui.wx.wxButton import WxButton
from util.observer import Subject


class Editor(wx.Panel, Subject):
    def __init__(self, parent):
        super(Editor, self).__init__(name="editorPanel",
                                     parent=parent)
        Subject.__init__(self)
        self.controls = ControlManager()
        self.controls.dump_json(stdin)
        self.selected_control = None

    def add_control(self, control, **attr):
        new_btn = control(self, **attr)
        self.controls.add(new_btn)
        self.controls.dump_json(stdin)

    def notify_properties(self):
        self.notify_all()

    def notify_all(self):
        for o in self.observers:
            o.notify({'selected_control': self.selected_control })