"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/2/15
"""
import wx

from layout_editor import dialogs
from layout_editor.Editor import Editor
from ui.wx.wxButton import WxButton
from ui.wx.wxLabel import WxLabel
from util.observer import Subject
from windows import *


class LayoutEditor(wx.Frame):
    def __init__(self, parent):
        super(LayoutEditor, self).__init__(parent=parent,
                                           title="Layout Editor",
                                           size=(640, 480))
        self.editor = None
        self.selected_control = None
        self.props = None
        self.initUi()
        self.Center()
        self.Show()

    def initUi(self):
        self.SetMenuBar(self.create_menu())
        self.editor = Editor(self)
        self.props = PropertiesWindow(self)
        self.editor.add(self.props)

    def create_menu(self):
        menu = wx.MenuBar()
        menu.Append(*self.create_file_menu())
        menu.Append(*self.create_controls_menu())
        menu.Append(*self.create_help_menu())
        return menu

    def create_file_menu(self):
        file_menu = wx.Menu()
        itmNew = file_menu.Append(wx.ID_NEW, 'New UI...', 'New Document')
        itmOpen = file_menu.Append(wx.ID_OPEN, 'Open...', 'Open Document')
        itmSave = file_menu.Append(wx.ID_SAVE, 'Save', 'Save Document')
        itmSaveAs = file_menu.Append(wx.ID_SAVEAS, 'Save As...', 'Save As')
        file_menu.AppendSeparator()
        itmQuit = file_menu.Append(wx.ID_EXIT, 'Quit', 'Quit Application')
        self.Bind(wx.EVT_MENU, lambda e: self.new_document(), itmNew)
        self.Bind(wx.EVT_MENU, lambda e: self.open_document(), itmOpen)
        self.Bind(wx.EVT_MENU, lambda e: self.save_document(), itmSave)
        self.Bind(wx.EVT_MENU, lambda e: self.save_document(True), itmSaveAs)
        self.Bind(wx.EVT_MENU, self.quit, itmQuit)
        return file_menu, '&File'

    def create_controls_menu(self):
        controls_menu = wx.Menu()
        itm_add_lbl = controls_menu.Append(wx.ID_ANY, 'Add Label...')
        itm_add_btn = controls_menu.Append(wx.ID_ANY, 'Add Button...')
        self.Bind(wx.EVT_MENU,
                  lambda e: self.add_control(WxLabel),
                  itm_add_lbl)
        self.Bind(wx.EVT_MENU,
                  lambda e: self.add_control(WxButton),
                  itm_add_btn)
        return controls_menu, '&Add Controls'

    def add_control(self, t):
        add_dialog = dialogs.ControlAttrDialog(t, parent=None,
                                               title='Add Control')
        res = add_dialog.ShowModal()
        if res == wx.ID_OK:
            width = 0
            height=0
            if t == WxButton:
                width = int(add_dialog.txt_width.Value)
                height = int(add_dialog.txt_height.Value)
            text = add_dialog.txt_text.Value
            self.editor.add_control(t, x=0, y=0,
                                    width=width, height=height,
                                    text=text)
        add_dialog.Destroy()

    def create_help_menu(self):
        help_menu = wx.Menu()
        itmHelp = help_menu.Append(wx.ID_HELP, 'Help', 'Help')
        help_menu.AppendSeparator()
        itmAbout = help_menu.Append(wx.ID_ABOUT, 'About', 'About')
        self.Bind(wx.EVT_MENU, lambda e: self.open_help(), itmHelp)
        self.Bind(wx.EVT_MENU, lambda e: self.open_about(), itmAbout)
        return help_menu, '&Help'

    def new_document(self):
        print('Creating new document...')

    def open_document(self):
        print('Opening document...')

    def save_document(self, save_as=False):
        print('Saving document%s...' % (' as ' if save_as else ''))

    def open_about(self):
        print('Opening about form...')

    def open_help(self):
        print('Opening help form...')

    def quit(self, e):
        self.Close()

    def notify_props(self):
        self.props.make_selected(self.selected_control)

if __name__ == '__main__':
    app = wx.App()
    LayoutEditor(None)
    app.MainLoop()