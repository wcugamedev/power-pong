"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""


class GameState:
    def __init__(self, ID, state_engine):
        self.ID = ID
        self.init(state_engine)

    def init(self, state_engine):
        raise NotImplementedError()

    def render(self, display):
        raise NotImplementedError()

    def update(self, state_engine):
        raise NotImplementedError()

    def on_enter(self):
        pass

    def on_exit(self):
        pass