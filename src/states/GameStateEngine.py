"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""


class GameStateEngine:
    def __init__(self, parent_game):
        self.states = {}
        self.current = None
        self.parent_game = parent_game

    def render(self, display):
        if self.current:
            self.current.render(display)

    def update(self):
        if self.current:
            self.current.update(self)

    def add(self, state, transfer=False):
        self.states[state.ID] = state
        if transfer:
            self.change(state.ID)

    def change(self, ID):
        if ID in self.states:
            if self.current:
                self.current.on_exit()
            self.current = self.states[ID]
            self.current.on_enter()
        else:
            print("ERR: Invalid game state %d" % ID)

    def quit(self):
        self.parent_game.running = False

    def get(self, ID):
        if ID in self.states:
            return self.states[ID]
        else:
            print("Cannot get invalid id %d..." % ID)