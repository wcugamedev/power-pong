"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""
from states.GameState import GameState
from states.States import States
from ui.ControlManager import ControlManager


class TestState(GameState):
    def __init__(self, state_engine):
        GameState.__init__(self, States.TEST_STATE, state_engine)

    def init(self, state_engine):
        self.controls = ControlManager()

    def render(self, display):
        self.controls.render(display)

    def update(self, state_engine):
        self.controls.update()