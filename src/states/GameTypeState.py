"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/26/14
"""
import pygame
from objects.GameType import GameType
from states.GameState import GameState
from states.States import States
from ui.LayoutManager import LayoutManager


class GameTypeState(GameState):
    def __init__(self, state_engine):
        GameState.__init__(self, States.GAME_TYPE_STATE, state_engine)
        self.mouse_down = True

    def init(self, state_engine):
        self.controls = \
            LayoutManager('../res/game_selection_controls.json').controls
        self.controls.get('btnPvp').on_mouse = \
            lambda: self.pvp_init(state_engine)
        self.controls.get('btnPvc').on_mouse = \
            lambda: self.pvc_init(state_engine)
        self.controls.get('btnCvc').on_mouse = \
            lambda: self.cvc_init(state_engine)

    def update(self, state_engine):
        if pygame.mouse.get_pressed()[0] == 0:
            self.mouse_down = False

        if not self.mouse_down:
            self.controls.update()

    def render(self, display):
        self.controls.render(display)

    def pvp_init(self, state_engine):
        state_engine.get(States.GAMEPLAY_STATE).game_type = GameType.PVP
        state_engine.get(States.GAMEPLAY_STATE).init(state_engine)
        state_engine.change(States.GAMEPLAY_STATE)

    def pvc_init(self, state_engine):
        state_engine.get(States.GAMEPLAY_STATE).game_type = GameType.PVC
        state_engine.get(States.GAMEPLAY_STATE).init(state_engine)
        state_engine.change(States.GAMEPLAY_STATE)

    def cvc_init(self, state_engine):
        state_engine.get(States.GAMEPLAY_STATE).game_type = GameType.CVC
        state_engine.get(States.GAMEPLAY_STATE).init(state_engine)
        state_engine.change(States.GAMEPLAY_STATE)
