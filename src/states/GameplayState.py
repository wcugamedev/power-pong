"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""
from objects.GameType import GameType
from objects.Ball import Ball
from objects.PlayArea import PlayArea
from objects.PlayerManager import PlayerManager
from powerups.PowerUpManager import PowerUpManager
from states.States import States
from states.GameState import GameState
from ui.ControlManager import ControlManager
from ui.LayoutManager import LayoutManager
from util.observer import Observer


class GameplayState(GameState, Observer):
    def __init__(self, state_engine):
        self.game_type = GameType.PVP
        GameState.__init__(self, States.GAMEPLAY_STATE, state_engine)

    def init(self, state_engine):
        self.play_area = PlayArea(620, 440)
        self.ball = Ball(self.play_area)
        self.players = PlayerManager(self.game_type, self.play_area, self.ball)
        self.power_ups = PowerUpManager(self.players, self.ball, self.play_area)
        self.ball.player1 = self.players.player1
        self.ball.player2 = self.players.player2
        self.has_won = False
        self.winner = None
        self.msg_duration = 3000
        self.msg_timer = 0
        self.msg_showing = False
        self.round_end = False
        self.msg_controls = ControlManager()
        self.controls = ControlManager()
        self.create_controls()
        self.set_up_listeners()

    def create_controls(self):
        self.controls_layout = LayoutManager('../res/game_play_controls.json')
        self.controls = self.controls_layout.controls
        self.msg_controls_layout = \
            LayoutManager('../res/game_play_msg_controls.json')
        self.msg_controls = self.msg_controls_layout.controls

    def set_up_listeners(self):
        self.ball.add(self.players)
        self.players.add(self)

    def reset_round(self):
        self.ball.init()
        self.round_end = False

    def update(self, state_engine):
        if self.has_won:
            if not self.msg_showing:
                self.display_message("Winner is %s" % self.winner.name, 200)
            self.msg_timer -= 1
            if self.msg_timer <= 0:
                state_engine.change(States.MAIN_MENU_STATE)
        elif self.round_end:
            self.reset_round()
        else:
            self.players.update()
            self.ball.update()
            self.power_ups.update()

    def render(self, display):
        if not self.has_won:
            self.players.render(display)
            self.ball.render(display)
            self.controls.render(display)
            self.play_area.render(display)
            self.power_ups.render(display)
        else:
            if self.msg_timer > 0:
                self.msg_controls.render(display)

    def notify(self, evt_args):
        if 'winner' in evt_args:
            winner = evt_args['winner']
            if winner.points >= 10:
                self.has_won = True
                self.winner = winner
            else:
                self.round_end = True
            if winner == self.players.player1:
                self.controls.get('lblPlayer1').text = str(winner.points)
            else:
                self.controls.get('lblPlayer2').text = str(winner.points)

    def display_message(self, msg, duration):
        if not self.msg_controls.get('lblMessage').text == msg:
            self.msg_controls.get('lblMessage').text = msg
            self.msg_controls.get('lblMessage').width = \
                self.msg_controls.get('lblMessage').font.size(msg)[0]
            self.msg_controls.get('lblMessage').height = \
                self.msg_controls.get('lblMessage').font.size(msg)[1]
            self.msg_controls.get('lblMessage').x = \
                640 / 2 - self.msg_controls.get('lblMessage').width / 2
            self.msg_controls.get('lblMessage').y = 20
        self.msg_showing = True
        self.msg_duration = duration
        self.msg_timer = self.msg_duration
