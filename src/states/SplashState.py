"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""
import pygame
from states.States import States
from states.GameState import GameState


class SplashState(GameState):
    def __init__(self, state_engine):
        GameState.__init__(self, States.SPLASH_STATE, state_engine)

    def init(self, state_engine):
        # Amount of ms the splash screen is displayed
        self.duration = 2000
        self.start = pygame.time.get_ticks()
        self.timer = self.start
        self.title_image = pygame.image.load('../res/powerpong.png')

    def render(self, display):
        display.blit(self.title_image, (0, 0))

    def update(self, state_engine):
        if self.timer < self.start + self.duration:
            self.timer = pygame.time.get_ticks()
        else:
            state_engine.change(States.MAIN_MENU_STATE)
