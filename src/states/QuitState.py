"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""
from states.States import States
from states.GameState import GameState


class QuitState(GameState):
    def __init__(self, state_engine):
        GameState.__init__(self, States.QUIT_STATE, state_engine)

    def init(self, state_engine): pass
    def render(self, display): pass
    def update(self, state_engine):
        state_engine.quit()