"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""
import pygame
from states.States import States
from states.GameState import GameState
from ui.LayoutManager import LayoutManager


class MainMenuState(GameState):
    def __init__(self, state_engine):
        GameState.__init__(self, States.MAIN_MENU_STATE, state_engine)

    def init(self, state_engine):
        self.layout = LayoutManager('../res/menu_controls.json')
        self.controls = self.layout.controls
        self.controls.get('btnPlay').on_mouse = \
            lambda: state_engine.change(States.GAME_TYPE_STATE)
        self.controls.get('btnQuit').on_mouse = lambda: state_engine.quit()

    def update(self, state_engine):
        self.controls.update()

        if pygame.key.get_pressed()[pygame.K_SEMICOLON] != 0:
            state_engine.change(States.TEST_STATE)

    def render(self, display):
        self.controls.render(display)