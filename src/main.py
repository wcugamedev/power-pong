"""
This module contains the main entry point into the game. It runs an instance
of PowerPong found in the PowerPong module.

:author: Anthony Benavente
:date: 12/2/14
"""

from game.PowerPong import PowerPong


def main():
    game = PowerPong()
    game.run()

if __name__ == '__main__':
    main()