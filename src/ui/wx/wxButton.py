"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/2/15
"""
import wx

from ui.wx.wxControl import WxControl
from util import color


class WxButton(WxControl):
    def __init__(self, parent, **kwargs):
        WxControl.__init__(self, parent, **kwargs)
        text = kwargs['text'] if 'text' in kwargs else ''
        self.text = text
        self.button = wx.Button(parent,
                                label=text,
                                pos=(self.x, self.y),
                                size=(self.width, self.height),
                                name=str(self.name))
        self.button.Bind(wx.EVT_LEFT_DOWN, self.on_mouse_down)
        self.button.Bind(wx.EVT_MOTION, self.on_mouse_move)

    def set_text(self, new_text):
        self.text = new_text
        self.button.SetLabel(self.text)

    def set_bg(self, bg):
        WxControl.set_bg(self, bg)
        self.button.SetForegroundColour(color.hex_to_color(bg))

    def set_image_path(self, image_path):
        WxControl.set_image_path(self, image_path)

    def set_font(self, font):
        WxControl.set_font(self, font)

    def set_y(self, y):
        WxControl.set_y(self, y)
        self.button.SetPosition((self.x, y))

    def set_x(self, x):
        WxControl.set_x(self, x)
        self.button.SetPosition((x, self.y))

    def set_fg(self, fg):
        WxControl.set_fg(self, fg)
        self.button.SetForegroundColour(color.hex_to_color(fg))

    def set_width(self, width):
        WxControl.set_width(self, width)
        self.button.SetSize((width, self.button.Size[1]))

    def set_height(self, height):
        WxControl.set_height(self, height)
        self.button.SetSize((self.button.Size[0], height))

    def set_name(self, name):
        self.name = name
        self.button.SetName(name)

    def on_mouse_down(self, e):
        self.dragging = not self.dragging
        self.button.Parent.selected_control = self
        self.button.Parent.notify_properties()

    def on_mouse_move(self, e):
        if self.dragging:
            self.x, self.y =\
                self.button.Parent.ScreenToClient(wx.GetMousePosition())
            self.x -= self.width / 2
            self.y -= self.height / 2
            self.button.SetPosition((self.x, self.y))

    def get_properties(self):
        props = WxControl.get_properties(self)
        props['text'] = self.text

