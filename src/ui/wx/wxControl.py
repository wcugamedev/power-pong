"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/2/15
"""
import wx
from ui.Control import Control


class WxControl(Control):
    def __init__(self, parent, **kwargs):
        Control.__init__(self, **kwargs)
        self.parent = parent
        self.dragging = False

    def init_image(self):
        pass

    def init_font(self):
        return None, 0

    def render(self, display):
        pass # Do nothing to render -- wx handles it all

    def update(self):
        pass # Do nothing to update -- wx handles it all
