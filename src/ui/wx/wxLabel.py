"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/4/15
"""
import wx

from ui.wx.wxControl import WxControl


class WxLabel(WxControl):
    def __init__(self, parent, **kwargs):
        WxControl.__init__(self, parent, **kwargs)
        self.label = wx.StaticText(parent, label=kwargs['text'])
        self.label.Bind(wx.EVT_LEFT_DOWN, self.on_mouse_down)
        self.label.Bind(wx.EVT_MOTION, self.on_mouse_move)
        self.width = self.label.Size[0]
        self.height = self.label.Size[1]

    def on_mouse_down(self, e):
        self.dragging = not self.dragging

    def on_mouse_move(self, e):
        if self.dragging:
            self.x, self.y =\
                self.label.Parent.ScreenToClient(wx.GetMousePosition())
            self.x -= self.width / 2
            self.y -= self.height / 2
            self.label.SetPosition((self.x, self.y))