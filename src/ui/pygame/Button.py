"""
This module contains the class definition for a generic button. Buttons can be
very basic with just a filled rectangle background and text or can have an
image be the background to look like a sophisticated UI.

:author: Anthony Benavente
:date: 12/22/14
"""
from ui.pygame.PygameControl import PygameControl


class Button(PygameControl):
    """
    This class represents a generic UI button. A button is a rectangular area
    that has text inside of it that fires an event when it is clicked.
    """

    def __init__(self, **kwargs):
        """
            The only extra argument this constructor takes is the text argument
            which is the text displayed inside of the button.

            :param text: The text to be displayed within the button.
            :param kwargs: See `Control.__init__`
            """
        PygameControl.__init__(self, **kwargs)
        text = kwargs['text'] if 'text' in kwargs else ''
        self.text = text

    def render(self, display):
        """
        Renders the button to the screen. If there is no background image, it
        fills the button with the background color of this control.

        :param display: The pygame surface to draw to
        """
        #TODO: Have multiple images (pressed, not-pressed)
        if self.image:
            self.draw_image(display)
        else:
            self.draw_back_color(display)
        padding_top = self.height / 2 - self.line_height / 2
        padding_left = self.width / 2 - self.font.size(self.text)[0] / 2
        display.blit(self.font.render(self.text, True, self.fore_color),
                         (self.x + padding_left, self.y + padding_top))

    def get_properties(self):
        props = PygameControl.get_properties(self)
        props['text'] = self.text
        props['instance'] = 'Button'
        return props