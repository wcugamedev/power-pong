"""
This module contains the class definition for a generic UI label.

:author: Anthony Benavente
:date: 12/22/14
"""
from ui.Control import Control
from ui.pygame.PygameControl import PygameControl


class Label(PygameControl):
    """
    This class represents a UI label which is just a container to display text.
    """
    def __init__(self,**kwargs):
        """
        This constructor takes text and another parameter in addition to the
        described parameters in Control.__init__.

        :param text: This is the text to be displayed in the label.
        :param show_background: This is a boolean (default is False) that
        :param kwargs:
        :return:
        """
        PygameControl.__init__(self, **kwargs)
        text = kwargs['text'] if 'text' in kwargs else ''
        show_background = kwargs['bg_show'] if 'bg_show' in kwargs else False
        self.text = text
        self.show_background = show_background
        self.size = self.width, self.height = self.font.size(self.text)

    def render(self, display):
        """
        Displays the label to the screen. If the show_background field was set
        to True, then the label will have a background color given by the the
        bg field.

        :param display: The pygame surface to draw this control to.
        """
        self.draw_image(display)
        if not self.show_background:
            display.blit(self.font.render(self.text, True, self.fore_color),
                         (self.x, self.y))
        else:
            display.blit(self.font.render(self.text, True, self.fore_color,
                                          self.back_color),
                (self.x, self.y))

    def get_properties(self):
        props = Control.get_properties(self)
        props['text'] = self.text
        props['bg_visible'] = self.show_background
        props['instance'] = 'Label'
        return props