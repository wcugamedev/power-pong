"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 1/2/15
"""
import pygame
from ui.Control import Control

DEFAULT_CURSOR = pygame.cursors.arrow


class PygameControl(Control):
    def __init__(self, **kwargs):
        Control.__init__(self, **kwargs)

    def init_font(self):
        if self.font_name:
            font = (pygame.font.Font(*self.font_name), self.font_name[1])
        else:
            font = (pygame.font.SysFont("Arial", 18), 18)
        return font

    def init_image(self):
        if self.image_path:
            self.image = pygame.image.load(self.image_path)

    def draw_image(self, display):
        """
        Draws the background image of this control to the passed in pygame
        surface.

        :param display: The pygame surface to draw to
        """
        if self.image:
            if not self.scaled_img:
                self.scaled_img = \
                    pygame.transform.scale(self.image,
                                           (self.width, self.height))
            display.blit(self.scaled_img, (self.x, self.y))

    def draw_back_color(self, display):
        """
        Draws a filled rectangle with the background color of this control to
        the specified pygame surface.

        :param display: The pygame surface to draw the rectangle to.
        """
        pygame.draw.rect(display, self.back_color,
            (self.x, self.y, self.width, self.height))

    def update(self):
        """
        Checks with pygame to see if the user has interacted with this control
        """
        if self.contains(pygame.mouse.get_pos()):
            # pygame.mouse.set_cursor(*self.cursor)
            if pygame.mouse.get_pressed()[0] and self.on_mouse:
                self.on_mouse()
        # else:
        #     pygame.mouse.set_cursor(*DEFAULT_CURSOR)

        if self.focused:
            if sum(pygame.key.get_keys(), 0) > 0:
                if self.on_key:
                    self.on_key()