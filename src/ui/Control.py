"""
This module contains the base class for UI controls.

:author: Anthony Benavente
:date: 12/22/14
"""
from util import color


class Control:
    """
    This class is the base class for all controls. It also contains helper
    methods to check user input with pygame.
    """
    def __init__(self, **kwargs):
        """
        All the below parameters are kwargs accepted by this constructor.

        :param x: Keyword argument. Sets the x coordinate of the control
        :param y: Keyword argument. Sets the y coordinate of the control
        :param width: Keyword argument. Sets the width of the collision box for
            the control
        :param height: Keyword argument. Sets the height of the collision box
            for the control
        :param bg: Keyword argument. Sets the background color of the control
        :param fg: Keyword argument. Sets the foreground (font) color of the
            control
        :param on_mouse: Keyword argument. Sets the event that occurs when the
            mouse is clicked in this control
        :param on_key: Keyword argument. Sets the event that occurs when a key
            is pressed while this control is focused
        :param font: Keyword argument. Sets the font of the text displayed in
            this control. A tuple containing the font name and the size
        :param image: Keyword argument. Sets the background image of this
            control
        """
        x = 0 if not 'x' in kwargs else kwargs['x']
        y = 0 if not 'y' in kwargs else kwargs['y']
        width = 0 if not 'width' in kwargs else kwargs['width']
        height = 0 if not 'height' in kwargs else kwargs['height']
        bg = color.white if not 'bg' in kwargs else kwargs['bg']
        fg = color.black if not 'fg' in kwargs else kwargs['fg']
        on_mouse = None if not 'on_mouse' in kwargs else kwargs['on_mouse']
        on_key = None if not 'on_key' in kwargs else kwargs['on_key']
        font = ('Arial', 18) if not 'font' in kwargs else \
            kwargs['font']
        name = hash(self) if not 'name' in kwargs else kwargs['name']
        image_path = None if not 'image' in kwargs else kwargs['image']
        line_height = 0 if not 'line_height' in kwargs else\
            kwargs['line_height']
        self.pos = self.x, self.y = x, y
        self.size = self.width, self.height = width, height
        self.back_color = bg
        self.fore_color = fg
        self.on_mouse = on_mouse
        self.on_key = on_key
        self.font_name = font
        self.image_path = image_path
        self.focused = False
        self.name = name
        self.line_height = line_height
        self.image = self.init_image()
        self.font, self.line_height = self.init_font()

    def init_image(self):
        raise NotImplementedError()

    def init_font(self):
        raise NotImplementedError()

    def update(self):
        raise NotImplementedError()

    def render(self, display):
        """
        A method that must be implemented that renders the control to the
        screen.

        :param display: The pygame surface that this control will be drawn to
        """
        raise NotImplementedError()

    def contains(self, point):
        """
        Checks if the passed in point is inside of this control's bounding box.

        :param point: The point to check
        :return: True or false if the point is inside this control or not
        """
        return self.x <= point[0] <= self.x + self.width and \
               self.y <= point[1] <= self.y + self.height

    def get_properties(self):
        return {
            'x':self.x,
            'y':self.y,
            'width': self.width,
            'height': self.height,
            'bg': self.back_color,
            'fg':self.fore_color,
            'image': self.image_path,
            'focused': self.focused,
            'font': self.font_name,
            'name': self.name
        }

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def set_width(self, width):
        self.width = width

    def set_height(self, height):
        self.height = height

    def set_bg(self, bg):
        self.bg = bg

    def set_fg(self, fg):
        self.fg = fg

    def set_font(self, font):
        self.font_name = font
        self.font = self.init_font()

    def set_image_path(self, image_path):
        self.image_path = image_path
        self.image = self.init_image()

    def set_name(self, name):
        self.name = name
