"""
This module contains the class definition for a ControlManager.

:author: Anthony Benavente
:date: 12/22/14
"""
import json
import sys


class ControlManager:
    """
    This class represents a collection of controls that provide utilities for
    updating, rendering, adding, and removing controls.
    """
    def __init__(self):
        self.controls = []
        self.name_control_map = {}

    def add(self, control):
        """
        Adds the passed in control to the list of controls

        :param control: The control to add to the control list
        """
        #TODO: Verify types
        if not control.name in self.name_control_map:
            self.controls.append(control)
            self.name_control_map[control.name] = control
        else:
            print('Control already exists: ' + control.name)

    def remove(self, control):
        """
        Removes the passed in control from the list of controls

        :param control: The control to remove from the control list
        """
        #TODO: Verify types
        #TODO: Verify existence of control
        self.controls.remove(control)

    def update(self):
        """
        Updates each control inside the control list
        """
        for c in self.controls:
            c.update()

    def render(self, display):
        """
        Draws each control inside the control list to the specified pygame
        surface.

        :param display: The pygame surface to draw the controls to.
        """
        for c in self.controls:
            c.render(display)

    def get(self, name):
        return self.name_control_map[name]

    def dump_json(self, filename):
        props = []
        for c in self.controls:
            props.append(c.get_properties())
        if type(filename) == str:
            with open(filename, 'w') as fp:
                fp.write(json.dumps(props))
        elif filename == sys.stdin:
            print json.dumps(props)