"""
This module contains the necessary components to have a file-based UI system

:author: Anthony Benavente
:date: 1/2/15
"""
import json

from ui.ControlManager import ControlManager
from ui.pygame.Button import Button
from ui.pygame.Label import Label


def class_from_str(s):
    """
    Gets an instance of a class with the given class name as a string.

    :param s: The name of the class as a string
    :return: A new object with the specified type
    """
    return eval(s)


class LayoutManager:
    """
    This class contains a ControlManager that is controlled through a JSON
    file.
    """
    def __init__(self, filename):
        """
        Initializes the LayoutManager to a specified JSON file.

        :param filename: The name of the json file INCLUDING the extension
        """
        self.filename = filename
        self.controls = ControlManager()
        self.load_controls()

    def load_controls(self):
        """
        Loads the controls from the specified JSON file
        """
        for c in json.load(fp=open(self.filename, 'r')):
            self.controls.add(class_from_str(c['instance'])(**c))