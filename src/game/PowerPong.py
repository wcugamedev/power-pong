"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""
from game.GameWindow import GameWindow
from states.GameStateEngine import GameStateEngine
from states.GameTypeState import GameTypeState
from states.QuitState import QuitState
from states.SplashState import SplashState
from states.MainMenuState import MainMenuState
from states.GameplayState import GameplayState
from states.TestState import TestState

SIZE = WIDTH, HEIGHT = 640, 480


class PowerPong(GameWindow):
    def __init__(self):
        GameWindow.__init__(self, SIZE, title="Power Pong")
        self.state_engine = GameStateEngine(self)
        # self.state_engine.add(SplashState(self.state_engine), True)
        self.state_engine.add(MainMenuState(self.state_engine), True)
        self.state_engine.add(GameplayState(self.state_engine))
        self.state_engine.add(QuitState(self.state_engine))
        self.state_engine.add(TestState(self.state_engine))
        self.state_engine.add(GameTypeState(self.state_engine))

    def render(self, display):
        self.state_engine.render(display)

    def update(self):
        self.state_engine.update()