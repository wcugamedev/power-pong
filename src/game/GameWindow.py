"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/2/14
"""
import pygame
from pygame import locals
import sys


class GameWindow:
    def __init__(self, size, title="Default Game Title"):
        self.running = True
        self.size = size
        self.title = title
        pygame.init()

    def render(self, display):
        raise NotImplementedError("This method must be implemented")

    def update(self):
        raise NotImplementedError("This method must be implemented")

    def run(self):
        self.clock = pygame.time.Clock()
        self.window = pygame.display.set_mode(self.size)
        pygame.display.set_caption(self.title)
        while True:
            pygame.draw.rect(self.window, (0, 0, 0), (0, 0, self.size[0],
                self.size[1]))
            self.update()
            self.render(self.window)
            for evt in pygame.event.get():
                if evt.type == locals.QUIT or not self.running:
                    pygame.quit()
                    sys.exit(0)
            pygame.display.update()
            self.clock.tick(60)
