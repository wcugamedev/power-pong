"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/25/14
"""
import pygame
from util import color


class PlayArea:
    def __init__(self, width, height,
                 auto=True,
                 border_width=7,
                 pos=(0, 0),
                 margin=(0, 0, 0, 0),
                 padding=(0, 0, 0, 0)):
        self.width = width
        self.height = height
        self.margin = margin
        self.padding = padding
        self.border_width = border_width
        self.x = pos[0]
        self.y = pos[1]
        if auto:
            self.x = 640 / 2 - self.width / 2
            self.y = 480 / 2 - self.height / 2
            self.margin = self.padding = 0, 0, 0, 0

    def render(self, display):
        self.draw_borders(display)
        self.draw_mid_line(display)

    def draw_mid_line(self, display):
        pygame.draw.rect(display, color.white,
            (self.x + (self.width / 2),
            self.y,
            self.border_width,
            self.height))

    def draw_borders(self, display):
        # Top border
        pygame.draw.rect(display, color.white,
            (self.x,
             self.y - self.border_width,
             self.width,
             self.border_width))
        # Bottom border
        pygame.draw.rect(display, color.white,
            (self.x,
            self.y + self.height,
            self.width  ,
            self.border_width))
        # Left border
        pygame.draw.rect(display, color.white,
            (self.x - self.border_width,
            self.y - self.border_width,
            self.border_width,
            self.height + self.border_width * 2))
        # Right border
        pygame.draw.rect(display, color.white,
            (self.x + self.width,
            self.y - self.border_width,
            self.border_width,
            self.height + self.border_width * 2))