"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""


class GameObject:
    def __init__(self, **kwargs):
        x = kwargs['x'] if 'x' in kwargs else 0
        y = kwargs['y'] if 'y' in kwargs else 0
        width = kwargs['width'] if 'width' in kwargs else 0
        height = kwargs['height'] if 'height' in kwargs else 0
        self.pos = self.x, self.y = x, y
        self.size = self.width, self.height = width, height

    def update(self):
        pass

    def render(self, display):
        pass

    def set_size(self, size):
        self.size = size
        self.width, self.height = size[0], size[1]

    def set_pos(self, pos):
        self.pos = pos
        self.x, self.y = pos[0], pos[1]