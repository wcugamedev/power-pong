"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""
from objects.GameType import GameType
from objects.Paddle import HumanPaddle, CpuPaddle
from util.observer import Subject, Observer


class PlayerManager(Subject, Observer):
    def __init__(self, type, play_area, ball):
        Subject.__init__(self)
        self.play_area = play_area
        if type == GameType.PVP:
            self.player1 = HumanPaddle(True, play_area)
            self.player2 = HumanPaddle(False, play_area)
        elif type == GameType.PVC:
            self.player1 = HumanPaddle(True, play_area)
            self.player2 = CpuPaddle(False, play_area, ball)
        elif type == GameType.CVC:
            self.player1 = CpuPaddle(True, play_area, ball)
            self.player2 = CpuPaddle(False, play_area, ball)
        self.round_winner = None
        self.reset_player_pos()

    def update(self):
        self.player1.update()
        self.player2.update()

    def render(self, display):
        self.player1.render(display)
        self.player2.render(display)

    def notify(self, evt_args):
        vel_x = evt_args['vel_x']
        if vel_x < 0:
            self.round_winner = self.player2
        else:
            self.round_winner = self.player1
        self.round_winner.points += 1
        self.notify_all()

    def notify_all(self):
        for o in self.observers:
            o.notify({'winner': self.round_winner})

    def reset_player_pos(self):
        self.player1.reset_pos()
        self.player2.reset_pos()