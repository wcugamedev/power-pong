"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""
import pygame
import math
from random import random
from objects.GameObject import GameObject
from util import color
from util.observer import Subject


class Ball(GameObject, Subject):
    def __init__(self, play_area, **kwargs):
        GameObject.__init__(self, **kwargs)
        Subject.__init__(self)
        self.min_pt = (play_area.x, play_area.y)
        self.max_pt = (play_area.x + play_area.width,
                    play_area.y + play_area.height)
        self.play_area = play_area
        self.width = 10
        self.height = 10
        self.speed = 6
        self.speed_mod = 1
        self.player1 = None
        self.player2 = None
        self.epsilon_gap = 5
        self.vel_x = self.speed
        self.max_vel = 15
        self.init()

    def reset_pos(self):
        self.x = (self.max_pt[0] - self.min_pt[0]) / 2
        self.y = (self.max_pt[1] - self.min_pt[1]) / 2

    def init(self):
        self.reset_pos()
        angle = math.sin(random() * 2 * math.pi)
        self.vel_y = self.speed * angle

    def update(self):
        self.move()
        self.check_players()

    def move(self):
        self.x += self.vel_x
        self.y += self.vel_y
        if self.y <= self.min_pt[1]:
            self.y = self.min_pt[1]
            self.vel_y *= -1
        elif self.y > self.max_pt[1] - self.height:
            self.y = self.max_pt[1] - self.height
            self.vel_y *= -1
        if self.x + self.width < self.min_pt[0] or self.x > self.max_pt[0]:
            self.notify_all()
            self.vel_x *= -1

    def check_players(self):
        if self.vel_x < 0:
            # Moving towards player 1
            if self.y + self.height > self.player1.y and \
                self.y < self.player1.y + self.player1.height and \
                self.x < self.player1.x + self.player1.width and \
                self.x > self.player1.x + self.player1.width - self.epsilon_gap:
                self.x = self.player1.x + self.player1.width
                self.vel_x *= -1
                self.inc_vel_y(self.player1.vel_y)
        else:
            # Moving towards player 2
            if self.y + self.height > self.player2.y and \
                self.y < self.player2.y + self.player1.height and \
                self.x > self.player2.x - self.width and \
                self.x < self.player2.x + self.epsilon_gap:
                self.x = self.player2.x - self.width
                self.vel_x *= -1
                self.inc_vel_y(self.player2.vel_y)

    def inc_vel_y(self, amt):
        self.vel_y += amt / 2
        if self.vel_y < -self.max_vel:
            self.vel_y = -self.max_vel
        elif self.vel_y > self.max_vel:
            self.vel_y = self.max_vel

    def render(self, display):
        pygame.draw.rect(display, color.white,
                         (self.x, self.y, self.width, self.height))

    def notify_all(self):
        for o in self.observers:
            o.notify({'vel_x': self.vel_x})