"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/23/14
"""
import pygame
import random
from objects.GameObject import GameObject
from util import color
from util.observer import Observer, Subject


class Paddle(GameObject):
    def __init__(self, is_player_1, play_area, **kwargs):
        GameObject.__init__(self, **kwargs)
        self.min_pt = (play_area.x, play_area.y)
        self.max_pt = (play_area.x + play_area.width,
                    play_area.y + play_area.height)
        self.play_area = play_area
        self.width = 20
        self.height = 60
        self.speed = 6
        self.speed_mod = 1
        self.points = 0
        self.is_player_1 = is_player_1
        self.vel_y = 0
        self.name = "Player 1" if is_player_1 else "Player 2"

    def reset_pos(self):
        if self.is_player_1:
            self.x = self.min_pt[0] + 5
        else:
            self.x = self.max_pt[0] - self.width - 5
        self.y = (self.min_pt[0] + self.play_area.height) / 2 - \
                 self.height / 2

    def update(self):
        raise NotImplementedError()

    def render(self, display):
        pygame.draw.rect(display, color.white,
                         (self.x, self.y, self.width, self.height))

    def move_up(self):
        self.set_pos((self.x, self.y + self.vel_y))
        if self.y < self.min_pt[1]:
            self.y = self.min_pt[1]

    def move_down(self):
        self.set_pos((self.x, self.y + self.vel_y))
        if self.y > self.max_pt[1] - self.height:
            self.y = self.max_pt[1] - self.height


class HumanPaddle(Paddle):
    def __init__(self, is_player_1, play_area, **kwargs):
        Paddle.__init__(self, is_player_1, play_area, **kwargs)
        self.up_key = pygame.K_w if is_player_1 else pygame.K_UP
        self.down_key = pygame.K_s if is_player_1 else pygame.K_DOWN

    def update(self):
        if pygame.key.get_pressed()[self.up_key]:
            self.vel_y = -self.speed
            self.move_up()
        if pygame.key.get_pressed()[self.down_key]:
            self.vel_y = self.speed
            self.move_down()

        if not pygame.key.get_pressed()[self.up_key] and \
            not pygame.key.get_pressed()[self.down_key]:
            self.vel_y = 0


class CpuPaddle(Paddle):
    def __init__(self, is_player_1, play_area, ball, **kwargs):
        Paddle.__init__(self, is_player_1, play_area, **kwargs)
        self.ball = ball
        self.generate_new_dev = 400
        self.timer = 0
        self.rand_dev = 0
        self.p_point = (0, 0)

    def update(self):
        b = self.ball
        if b.vel_x > 0 and b.x > self.play_area.width / 4 and \
                not self.is_player_1:
            self.p_point = p_point = self.predict_ball_pos((b.x, b.y),
                                            (b.vel_x, b.vel_y),
                                            (b.width, b.height))
            p_point = (p_point[0], p_point[1] - self.height / 2)
            if abs(b.x - self.x) < b.width + self.width * 1.25:
                swipe = random.random() < .2
                if swipe:
                    direction = self.ball.vel_y / abs(self.ball.vel_y)
                    self.vel_y = (self.speed * 3 / 4) * direction
            else:
                if abs(p_point[1] - self.y) > 5:
                    if self.y < p_point[1]:
                        self.vel_y = self.speed
                    else:
                        self.vel_y = -self.speed
                else:
                    self.vel_y = 0
        elif b.vel_x < 0 and b.x < 3 * self.play_area.width / 4 and \
                self.is_player_1:
            self.p_point = p_point = self.predict_ball_pos((b.x, b.y),
                                            (b.vel_x, b.vel_y),
                                            (b.width, b.height))
            p_point = (p_point[0], p_point[1] - self.height / 2)
            if abs(b.x - self.x) < b.width + self.width * 1.25:
                swipe = random.random() < .2
                if swipe:
                    direction = self.ball.vel_y / abs(self.ball.vel_y)
                    self.vel_y = (self.speed * 3 / 4) * direction
            else:
                if abs(p_point[1] - self.y) > 5:
                    if self.y < p_point[1]:
                        self.vel_y = self.speed
                    else:
                        self.vel_y = -self.speed
                else:
                    self.vel_y = 0
        self.y += self.vel_y
        if self.y < self.play_area.y:
            self.y = self.play_area.y
        elif self.y > self.play_area.y + self.play_area.height - self.height:
            self.y = self.play_area.y + self.play_area.height - self.height

    def render(self, display):
        Paddle.render(self, display)
        # pygame.draw.rect(display, color.green,
        #                  (self.p_point[0], self.p_point[1], 10, 10))

    def ball_in_range(self):
        return self.ball.y - self.ball.height - self.rand_dev > self.y and \
            self.ball.y < self.y + self.height + self.rand_dev

    def predict_ball_pos(self, ball_pos, ball_vel, ball_size):
        b_x, b_y = ball_pos
        b_vx, b_vy = ball_vel
        b_w, b_h = ball_size
        time_a = (self.x - b_x + b_w) / b_vx
        predict_y = (b_vy * time_a) + b_y
        predict_x = self.x

        if predict_y < 0:
            predict_y = 0
        elif predict_y > self.play_area.height - b_h:
            predict_y = self.play_area.height - b_h
        else:
            return predict_x, predict_y

        time_b = (predict_y - b_y) / b_vy
        predict_x = (b_vx * time_b) + b_x
        return self.predict_ball_pos((predict_x, predict_y),
                                     (b_vx, -b_vy),
                                     (b_w, b_h))