Power Pong
===

This game is a take on pong that includes the use of power ups and other
twists.

## Setting Up
This project relies on two main dependencies.

  1. [Pygame](http://www.pygame.org/download.shtml)
  2. [WxPython](http://www.wxpython.org/download.php)
  
Be sure to choose everything for Python 2.7! You will run into problems if you
install these libraries for any other version of Python. Just follow the 
instructions given on the site on how to install the dependencies and you should
be able to run it without issues.

## Running the Project
There are 2 entry points to the project. One of them is in the `main` module
in the src directory. This runs Power Pong like a normal video game executable.
The second entry point is located in the `LayoutEditor` module under 
src/layout_editor. This is the non-finished gui constructor. There are no
parameters for both of these entry points so they can be run by just invoking
them with Python like so:

    $ python main.py
    
or

    $ python LayoutEditor.py
    
##Contributors
  - Anthony Benavente
  - Daniel Powell
  - Cody Pero
