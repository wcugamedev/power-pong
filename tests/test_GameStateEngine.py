"""
TODO: Put a nice description here...

:author: Anthony Benavente
:date: 12/22/14
"""

import unittest
from states.GameState import GameState
from states.GameStateEngine import GameStateEngine


class TestGameStateEngine(unittest.TestCase):
    def setUp(self):
        self.state_engine = GameStateEngine()

    def test_add(self):
        class TestState1(GameState):
            def __init__(self):
                GameState.__init__(self, 0)
            def update(self, state_engine):
                print("Hi, I'm updating!")
            def render(self, display):
                print("Hi, I'm rendering!")
        class TestState2(GameState):
            def __init__(self):
                GameState.__init__(self, 1)
            def update(self, state_engine):
                print("Hi, I'm updating 2!")
            def render(self, display):
                print("Hi, I'm rendering 2!")
        state1 = TestState1()
        state2 = TestState2()
        self.state_engine.add(state1)
        self.state_engine.add(state2, True)
        self.state_engine.update()
        self.state_engine.render(None)
        self.assertDictEqual(self.state_engine.states,
            { 0: state1, 1: state2 })

if __name__ == '__main__':
    unittest.main()