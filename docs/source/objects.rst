objects package
===============

Module contents
---------------

.. automodule:: objects
    :members:
    :undoc-members:
    :show-inheritance:
