powerups package
================

Submodules
----------

powerups.PowerUp module
-----------------------

.. automodule:: powerups.PowerUp
    :members:
    :undoc-members:
    :show-inheritance:

powerups.PowerUpManager module
------------------------------

.. automodule:: powerups.PowerUpManager
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: powerups
    :members:
    :undoc-members:
    :show-inheritance:
