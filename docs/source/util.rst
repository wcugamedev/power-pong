util package
============

Submodules
----------

util.color module
-----------------

.. automodule:: util.color
    :members:
    :undoc-members:
    :show-inheritance:

util.observer module
--------------------

.. automodule:: util.observer
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: util
    :members:
    :undoc-members:
    :show-inheritance:
